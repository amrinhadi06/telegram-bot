module gitlab.com/ackersonde/telegram-bot

go 1.20

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/juruen/rmapi v0.0.25
)

require (
	github.com/adrg/strutil v0.3.0 // indirect
	github.com/adrg/sysfont v0.1.2 // indirect
	github.com/adrg/xdg v0.4.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	github.com/stretchr/testify v1.8.2 // indirect
	github.com/unidoc/freetype v0.0.0-20220130190903-3efbeefd0c90 // indirect
	github.com/unidoc/pkcs7 v0.1.1 // indirect
	github.com/unidoc/timestamp v0.0.0-20200412005513-91597fd3793a // indirect
	github.com/unidoc/unipdf/v3 v3.45.0 // indirect
	github.com/unidoc/unitype v0.4.0 // indirect
	golang.org/x/crypto v0.8.0 // indirect
	golang.org/x/image v0.7.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	golang.org/x/xerrors v0.0.0-20220907171357-04be3eba64a2 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
